import Vue from 'vue'
import store from '../store'

export default {
  adminLogin (username, password, callback) {
    Vue.$http({
      method: 'post',
      url: '/admin',
      headers: {'Content-Type': 'application/json'},
      data: {
        'username': username,
        'password': password
      }
    })
    .then(function (response) {
      var data = []
      data.push(response.data.id)
      data.push(response.data.user)
      data.push(response.data.jwt)
      store.dispatch('adminLogin', data)
      callback(null)
    }).catch(error => {
      callback(error.response.data.message)
    })
  }
}
