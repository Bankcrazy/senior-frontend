import Vue from 'vue'
import store from '../store'

export default {
  checkout (name,addr,postcode,district,province,tel,cID,total,cart,callback) {
    var itemList = []
    for (var i in cart){
      itemList.push({
        productId: cart[i].productId,
        amount: cart[i].quantity
      })
    }

    Vue.$http({
      method: 'post',
      url: '/checkout',
      headers: {'Content-Type': 'application/json'},
      data: {
          'status': 'Placed',
          'name': name,
          'address': addr,
          'postcode': postcode,
          'district': district,
          'province': province,
          'phoneNumber': tel,
          'total': total,
          'customerId': cID,
          'items': itemList
      }
    })
    .then(function (response) {
      var data = []
      store.dispatch('clearCart')
      data.push("OK")
      data.push(response.data)
      callback(data)
    }).catch(error => {
      store.dispatch('clearCart')
      var data = []
      data.push("ERROR")
      data.push(error.response.data.message)
      callback(data)
    })
  },
  getAllOrder (callback){
    Vue.$http({
      method: 'get',
      url: `/order`
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getOrder (id,callback){
    Vue.$http({
      method: 'get',
      url: `/order/${id}`,
      headers: {'Content-Type': 'application/json'}
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getOrderByCustomerId (customerId,callback){
    Vue.$http({
      method: 'get',
      url: `/order/customer/${customerId}`,
      headers: {'Content-Type': 'application/json'}
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  updateStatus (formData, callback){
    Vue.$http({
      method: 'post',
      url: '/order/update/status',
      data: formData
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getTotalStatus(formData, callback){
    Vue.$http({
      method: 'post',
      url: '/order/total/status',
      data: formData
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  }
}
