import Vue from 'vue'
import store from '../store'

export default {
  getAllProduct (callback) {
    Vue.$http({
      method: 'get',
      url: '/product',
      headers: {'Content-Type': 'application/json'}
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getProduct (id, callback){
    Vue.$http({
      method: 'get',
      url: `/product/${id}`,
      headers: {'Content-Type': 'application/json'}
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getProductSelectList (callback){
    Vue.$http({
      method: 'get',
      url: '/product/selectList',
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  addProduct (formData, callback){
    Vue.$http({
      method: 'post',
      url: 'secure/product/add',
      headers: {'authorization': 'Bearer '+store.getters.cookie},
      data: formData
    })
    .then(function (response) {
      var data = []
      data.push("OK")
      data.push(response.data)
      callback(data)
    }).catch(error => {
      var data = []
      data.push("ERROR")
      data.push(error.response.data.message)
      callback(data)
    })
  }
}
