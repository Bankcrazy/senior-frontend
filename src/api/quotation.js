import Vue from 'vue'
import store from '../store'

export default {
  getAllQuotation(callback) {
    Vue.$http({
      method: 'get',
      url: '/quotation',
      headers: {'Content-Type': 'application/json'}
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getQuotation (id, callback){
    Vue.$http({
      method: 'get',
      url: `/quotation/${id}`,
      headers: {'Content-Type': 'application/json'}
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getQuotationByCustomerId (customerId, callback){
    Vue.$http({
      method: 'get',
      url: `/quotation/customer/${customerId}`,
      headers: {'Content-Type': 'application/json'}
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  addQuotation (data, items, callback){
    Vue.$http({
      method: 'post',
      url: 'quotation/add',
      headers: {'Content-Type': 'application/json'},
      data: {
          'companyName': data[0],
          'companyBranch': data[1],
          'address': data[2],
          'postcode': data[3],
          'district': data[4],
          'province': data[5],
          'taxId': data[6],
          'contactName': data[7],
          'contactTel': data[8],
          'contactEmail': data[9],
          'remark': data[10],
          'status': data[11],
          'customerId': data[12],
          'totalItem': data[13],
          'totalQuantity': data[14],
          'items': items
      }
    })
    .then(function (response) {
      var data = []
      data.push("OK")
      data.push(response.data)
      callback(data)
    }).catch(error => {
      var data = []
      data.push("ERROR")
      data.push(error.response.data.message)
      callback(data)
    })
  },
  updateStatus (formData, callback){
    Vue.$http({
      method: 'post',
      url: '/quotation/update/status',
      data: formData
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getTotalStatus(formData, callback){
    Vue.$http({
      method: 'post',
      url: '/quotation/total/status',
      data: formData
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  updateOfferPrice (quotationId, items, callback){
    Vue.$http({
      method: 'post',
      url: '/quotation/update/offerPrice',
      data: {
        'quotationId': quotationId,
        'items': items
      }
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
}
