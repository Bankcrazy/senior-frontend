import Vue from 'vue'
import store from '../store'

export default {
  login (email, password, returnRoute, total, callback) {
    Vue.$http({
      method: 'post',
      url: '/login',
      headers: {'Content-Type': 'application/json'},
      data: {
        'email': email,
        'password': password
      }
    })
    .then(function (response) {
      var data = []
      data.push(response.data.id)
      data.push(response.data.user)
      data.push(response.data.jwt)
      data.push(returnRoute)
      data.push(total)
      store.dispatch('login', data)
      callback(null)
    }).catch(error => {
      callback(error.response.data.message)
    })
  },
  register (fn, ln, email, pwd, pwdc, callback) {
    Vue.$http({
      method: 'post',
      url: '/register',
      headers: {'Content-Type': 'application/json'},
      data: {
        'firstName': fn,
        'lastName': ln,
        'email': email,
        'password': pwd,
        'passwordConfirmation': pwdc
      }
    })
    .then(function (response) {
      callback(null)
    }).catch(error => {
      callback(error.response.data.message)
    })
  },
  getAllUser (callback) {
    Vue.$http({
      method: 'get',
      url: '/user',
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  },
  getUser (id, callback) {
    Vue.$http({
      method: 'get',
      url: `/user/${id}`,
    })
    .then(function (response) {
      callback(response.data)
    }).catch(function (response) {
      console.log(response)
    })
  }

}
