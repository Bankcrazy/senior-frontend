import Vue from 'vue'
import App from './App.vue'
import router from './router'

import Vuetify from 'vuetify'
import './stylus/main.styl'
import Axios from 'axios'
import store from './store'

Vue.use(Vuetify)
Vue.$http = Axios
Axios.defaults.baseURL = 'http://api.veeravit.club/'
Axios.defaults.headers.common.Accept = 'application/json'
Axios.defaults.withCredentials = true

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
