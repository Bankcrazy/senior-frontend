import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
// Shared Section
import ProductIndex from '../component/Product/Index'
import OrderIndex from '../component/Order/Index'
import QuotationIndex from '../component/Quotation/Index'
// User section
import HomeIndex from '../component/Home/Index'
import LoginIndex from '../component/Login/Index'
import RegisterIndex from '../component/Register/Index'
import CartIndex from '../component/Cart/Index'
import CartCheckout from '../component/Cart/Checkout'
import AccountIndex from '../component/Account/Index'
import AccountMyOrder from '../component/Account/MyOrder'
import AccountMyQuotation from '../component/Account/MyQuotation'
import AccountMyAccount from '../component/Account/MyAccount'
import AddQuotation from '../component/Quotation/AddQuotation'
// Admin Section
import AdminLogin from '../component/Admin/Login'
import AdminHome from '../component/Admin/Home'
import AdminOrder from '../component/Admin/Order'
import AdminProduct from '../component/Admin/Product'
import AdminAddProduct from '../component/Admin/AddProduct'
import AdminQuotation from '../component/Admin/Quotation'
import AdminUser from '../component/Admin/User'

Vue.use(Router)

const routes = [
  // User Section
  {
    path: '/',
    name: 'Home.index',
    component: HomeIndex
  },
  {
    path: '/product/:productId',
    name: 'Product.index',
    component: ProductIndex,
    props: true
  },
  {
    path: '/login',
    name: 'Login.index',
    component: LoginIndex,
    props: true
  },
  {
    path: '/register',
    name: 'Register.index',
    component: RegisterIndex
  },
  {
    path: '/cart',
    name: 'Cart.index',
    component: CartIndex
  },
  {
    path: '/checkout',
    name: 'Cart.checkout',
    component: CartCheckout
  },
  {
    path: '/account',
    name: 'Account.index',
    component: AccountIndex
  },
  {
    path: '/account/info',
    name: 'Account.myAccount',
    component: AccountMyAccount
  },
  {
    path: '/account/order',
    name: 'Account.myOrder',
    component: AccountMyOrder
  },
  {
    path: '/account/order/:orderId',
    name: 'Account.myOrderId',
    component: OrderIndex,
    props: true
  },
  {
    path: '/account/quotation',
    name: 'Account.myQuotation',
    component: AccountMyQuotation
  },
  {
    path: '/account/quotation/:quoteId',
    name: 'Account.myQuotationId',
    component: QuotationIndex,
    props: true
  },
  {
    path: '/quotation',
    name: 'Quotation.add',
    component: AddQuotation
  },
  // Admin Section
  {
    path: '/admin',
    name: 'Admin.login',
    component: AdminLogin
  },
  {
    path: '/admin/home',
    name: 'Admin.home',
    component: AdminHome
  },
  {
    path: '/admin/product',
    name: 'Admin.product',
    component: AdminProduct
  },
  {
    path: '/admin/product/detail/:productId',
    name: 'Admin.productId',
    component: ProductIndex,
    props: true
  },
  {
    path: '/admin/product/add',
    name: 'Admin.addProduct',
    component: AdminAddProduct
  },
  {
    path: '/admin/order',
    name: 'Admin.order',
    component: AdminOrder
  },
  {
    path: '/admin/order/:orderId',
    name: 'Admin.orderDetail',
    component: OrderIndex,
    props: true
  },
  {
    path: '/admin/quotation',
    name: 'Admin.quotation',
    component: AdminQuotation
  },
  {
    path: '/admin/quotation/:quoteId',
    name: 'Admin.quotationDetail',
    component: QuotationIndex,
    props: true
  },
  {
    path: '/admin/customer',
    name: 'Admin.user',
    component: AdminUser
  }
]

const router = new Router({
    routes
});

function checkNoAuthRoute(route){
  const routes = [
    'Home.index',
    'Product.index',
    'Login.index',
    'Register.index',
    'Cart.index',
    'Admin.login'
  ]

  if (store.getters.loggedIn){
    if (store.getters.role == 'USER'){
      if (route == 'Login.index' || route == 'Register.index'){
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  } else {
    for (var i in routes){
      if (routes[i] == route){
        return true;
      }
    }
    return false;
  }
}

function checkUserRoute(route){
  const routes = [
    'Cart.checkout',
    'Account.index',
    'Account.myAccount',
    'Account.myOrder',
    'Account.myOrderId',
    'Account.myQuotation',
    'Account.myQuotationId',
    'Quotation.add'
  ]

  for (var i in routes){
    if (routes[i] == route){
      return true;
    }
  }
  return false;
}

function checkAdminRoute(route){
  const routes = [
    'Admin.home',
    'Admin.product',
    'Admin.productId',
    'Admin.addProduct',
    'Admin.order',
    'Admin.orderDetail',
    'Admin.quotation',
    'Admin.quotationDetail',
    'Admin.user'
  ]

  for (var i in routes){
    if (routes[i] == route){
      return true;
    }
  }
  return false;
}

router.beforeEach((to, from, next) => {
  if (checkNoAuthRoute(to.name)) {
    next()
  }
  else {
    if (store.getters.loggedIn){
      if (store.getters.role == 'USER'){
        if (checkUserRoute(to.name)){
          next()
        } else {
          next('/')
        }
      } else {
        if (checkAdminRoute(to.name)){
          next()
        } else {
          next('/admin/order')
        }
      }
    }
    else {
      next('/login')
    }
  }
});

export default router;
