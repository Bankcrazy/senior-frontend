import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'

Vue.use(Vuex)

// initial state of the app
const state = {
  auth: false,
  id: null,
  user: null,
  cookie: null,
  role: null,
  cart: [],
  total: null
}

// mutations are operations that actually mutates the state.
// never call this directly. these actions are only called by `actions` below.
const mutations = {
  // admin login
  adminLogin (state, data) {
    console.log('mutations adminLogin')
    state.auth = true
    state.id = data[0]
    state.user = data[1]
    state.cookie = data[2]
    state.role = "ADMIN"
    router.push({ name: 'Admin.order' })
  },
  // user login
  login (state, data) {
    console.log('mutations login')
    state.auth = true
    state.id = data[0]
    state.user = data[1]
    state.cookie = data[2]
    state.role = "USER"
    if (data[3]==null){
      router.push({ name: 'Home.index' })
    }
    else {
      router.push({ name: data[3] })
      state.total = data[4]
    }
  },
  logout (state) {
    console.log('mutations logout')
    state.auth = false
    state.id = null
    state.user = null
    state.role = null
    state.cookie = null
    state.cart = []
    state.total = null
    router.push({ name: 'Login.index' })
  },
  ADD_TO_CART(state, product) {
    const record = state.cart.find(target => target.productId === product.productId)

    if (!record) {
      state.cart.push({
        productId: product.productId,
        image: product.image,
        productName: product.productName,
        price: product.price,
        quantity: 1
      })
      router.push({ name: 'Cart.index' })
   } else {
     record.quantity++
     router.push({ name: 'Cart.index' })
   }
  },
  UPDATE_ITEM_IN_CART(state, data) {
    const record = state.cart.find(target => target.productId === data[0])
    record.quantity = data[1]
  },
  REMOVE_FROM_CART(state, pId) {
    const index = state.cart.findIndex(cart => cart.productId === pId)
    state.cart.splice(index, 1)
  },
  ADD_TOTAL_CART_PRICE(state, total){
    state.total = total
    router.push({ name: 'Cart.checkout' })
  },
  CLEAR_CART(state){
    console.log('mutations clearCart')
    state.cart = []
    state.total = null
  }
}

// operations that can be dispatched from other components.
// example `store.dispatch('login')` will call `login` action and then mutate the state using `mutations.login`
const actions = {
  logout: ({ commit }) => commit('logout'),
  login(context, data) { context.commit('login', data); },
  adminLogin(context, data) { context.commit('adminLogin', data); },
  addToCart(context, product) { context.commit('ADD_TO_CART', product); },
  updateItemInCart(context, data) {context.commit('UPDATE_ITEM_IN_CART', data)},
  removeFromCart(context, pId) { context.commit('REMOVE_FROM_CART', pId); },
  addTotalCartPrice(context, total) {context.commit('ADD_TOTAL_CART_PRICE', total)},
  clearCart: ({ commit }) => commit('CLEAR_CART')
}

// just getter functions.
const getters = {
  loggedIn: (state) => state.auth,
  id: (state) => state.id,
  user: (state) => state.user,
  cookie: (state) => state.cookie,
  role: (state) => state.role,
  cart: (state) => state.cart,
  inCart: (state) => state.cart.length,
  total: (state) => state.total
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
